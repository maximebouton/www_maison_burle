<a href ="index.html">

        ..          
       _||_______   
     .'/:/:||:\:\`. 
     :------------: 
     |  [] [] []  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |__ ___ ___  | 
     ||| | | |=|  | 
    _||| ~~~ |_|  |_
    ░░░░░░░░░░░░░░░░

</a>

# Début des travaux 

![1e_c_face         ](src/media/photos/1e_c_face.png)

![1e_ch_face        ](src/media/photos/1e_ch_face.png)

![1e_ch_pile        ](src/media/photos/1e_ch_pile.png)

![1e_c_pile         ](src/media/photos/1e_c_pile.png)

![1e_escalier       ](src/media/photos/1e_escalier.png)

![1e_face           ](src/media/photos/1e_face.png)

![1e_piece_face     ](src/media/photos/1e_piece_face.png)

![1e_piece_pile     ](src/media/photos/1e_piece_pile.png)

![1e_salle-manger_0 ](src/media/photos/1e_salle-manger_0.png)

![1e_salle-manger_1 ](src/media/photos/1e_salle-manger_1.png)

![1e_salle-manger_2 ](src/media/photos/1e_salle-manger_2.png)

![1e_salle-manger_3 ](src/media/photos/1e_salle-manger_3.png)

![2-3e_escalier_face](src/media/photos/2-3e_escalier_face.png)

![2-3e_escalier_pile](src/media/photos/2-3e_escalier_pile.png)

![2e_atelier_face   ](src/media/photos/2e_atelier_face.png)

![2e_atelier_pile   ](src/media/photos/2e_atelier_pile.png)

![2e_c_face         ](src/media/photos/2e_c_face.png)

![2e_ch0_face       ](src/media/photos/2e_ch0_face.png)

![2e_ch0_pile       ](src/media/photos/2e_ch0_pile.png)

![2e_ch1_face       ](src/media/photos/2e_ch1_face.png)

![2e_ch1_pile       ](src/media/photos/2e_ch1_pile.png)

![2e_c_pile         ](src/media/photos/2e_c_pile.png)

![2e_escalier_face  ](src/media/photos/2e_escalier_face.png)

![2e_escalier_pile  ](src/media/photos/2e_escalier_pile.png)

![2e_sdb            ](src/media/photos/2e_sdb.png)

![3e_c_face         ](src/media/photos/3e_c_face.png)

![3e_ch0_face       ](src/media/photos/3e_ch0_face.png)

![3e_ch0_pile       ](src/media/photos/3e_ch0_pile.png)

![3e_ch1_face       ](src/media/photos/3e_ch1_face.png)

![3e_ch1_pile       ](src/media/photos/3e_ch1_pile.png)

![3e_ch2_face       ](src/media/photos/3e_ch2_face.png)

![3e_ch2_pile       ](src/media/photos/3e_ch2_pile.png)

![3e_c_pile         ](src/media/photos/3e_c_pile.png)

![3e_face_face      ](src/media/photos/3e_face_face.png)

![3e_hall_pile      ](src/media/photos/3e_hall_pile.png)

![3e_sdb_douche_face](src/media/photos/3e_sdb_douche_face.png)

![3e_sdb_douche_pile](src/media/photos/3e_sdb_douche_pile.png)

![3e_sdb_face       ](src/media/photos/3e_sdb_face.png)

![3e_sdb_pile       ](src/media/photos/3e_sdb_pile.png)

![cour_appendice    ](src/media/photos/cour_appendice.png)

![cour_depuis2e     ](src/media/photos/cour_depuis2e.png)

![cour_facade       ](src/media/photos/cour_facade.png)

![facade            ](src/media/photos/facade.png)

![grenier_face      ](src/media/photos/grenier_face.png)

![grenier_fonds     ](src/media/photos/grenier_fonds.png)

![grenier_pile      ](src/media/photos/grenier_pile.png)

![rdc-1e_escalier   ](src/media/photos/rdc-1e_escalier.png)

![rdc_entree_face   ](src/media/photos/rdc_entree_face.png)

![rdc_entree_pile   ](src/media/photos/rdc_entree_pile.png)

![rdc_escalier      ](src/media/photos/rdc_escalier.png)

![rdc_piece0_face   ](src/media/photos/rdc_piece0_face.png)

![rdc_piece0_pile   ](src/media/photos/rdc_piece0_pile.png)

![rdc_piece1_face   ](src/media/photos/rdc_piece1_face.png)

![rdc_piece1_pile   ](src/media/photos/rdc_piece1_pile.png)

![rdc_piece2_face   ](src/media/photos/rdc_piece2_face.png)

![rdc_piece2_pile   ](src/media/photos/rdc_piece2_pile.png)

---

<a href ="index.html">

        ..          
       _||_______   
     .'/:/:||:\:\`. 
     :------------: 
     |  [] [] []  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |__ ___ ___  | 
     ||| | | |=|  | 
    _||| ~~~ |_|  |_
    ░░░░░░░░░░░░░░░░

</a>
