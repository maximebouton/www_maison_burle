<!--<header><a href=".">Accueil</a>-<a href=".">Chroniques</a>-<a href=".">Projets</a>-<a href=".">Qui sommes nous ?</a>-<a href=".">Contact</a></header>-->
# La Maison Burle

        ..          
       _||_______   
     .'/:/:||:\:\`. 
     :------------: 
     |  [] [] []  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |__ ___ ___  | 
     ||| | | |=|  | 
    _||| ~~~ |_|  |_
    ░░░░░░░░░░░░░░░░

## <span>Ateliers ouverts</span> & <span>pratiques éditoriales</span>
    
### Photo-impression, menuiserie, informatique

    1 place de la République, 07320 Saint-Agrève
 
---

## Plan


| Textes                             | Photos                                  |
|:----------------------------------:|:---------------------------------------:|
| [Présentation](#présentation)      | [Début des travaux](debut_travaux.html) |
| [Le mur mourant](mur_mourant.html) |                                         |

## Présentation    

Comment habiter, faire vivre l'existant, travailler ce qui disparait.

![La machine à laver la plus pratique et la moins chère du monde, Tricotons pour nous deux, Le Point Tapie l'homme qui veut tout, Échos du Chiniac, Le Dauphiné Aujourd'hui hommage à De Gaulle, Saphir beauté santé du cuir.](./src/media/photos/devanture.jpg)

Many options are available to Manu in order to give as much flexibility as possible to the user.  Changes can be made to the search path, section order, output processor, and other behaviours and operations detailed below.

If set, various environment variables are interrogated to determine the operation of man.  It is possible to set the `catch all variable $MANOPT` to any string in command line format with the exception that any spaces used  as
part  of  an  option's argument must be escaped (preceded by a backslash).  Manu will parse `$MANOPT` prior to parsing its own command line.  Those options requiring an argument will be overridden by the same options found on the
command line.  To reset all of the options set in `$MANOPT`, `-D` can be specified as the initial command line option.  This will allow Manu to `forget` about the options specified in `$MANOPT` although  they  must  still  have  been
valid.

The Manu pager utilities packaged as man-db make extensive use of index database caches.  These caches contain information such as where each Manu page can be found on the filesystem and what its whatis (short one line de‐
scription of the Manu page) contains, and allow Manu to run faster than if it had to search the filesystem each time to find the appropriate Manu page.  If requested using the -u option, Manu will ensure that the caches  remain
consistent, which can obviate the need to Manuly run software to update traditional whatis text databases.

If  Manu  cannot  find  a mandb initiated index database for a particular Manu page hierarchy, it will still search for the requested Manu pages, although file globbing will be necessary to search within that hierarchy.  If
whatis or apropos fails to find an index it will try to extract information from a traditional whatis database instead.

These utilities support compressed source nroff files having, by default, the extensions of `.Z`, `.z` and `.gz`.  It is possible to deal with any compression extension, but this information must be known at compile time.  Also,  by
default, any cat pages produced are compressed using gzip.  Each `global` Manu page hierarchy such as `/usr/share/man` or `/usr/X11R6/man` may have any directory as its cat page hierarchy.  Traditionally the cat pages are stored
under the same hierarchy as the Manu pages, but for reasons such as those specified in the File Hierarchy Standard (FHS), it may be better to store them elsewhere.  For details on how to do this, please  read  manpath(5).   For
details on why to do this, read the standard.

![Manu](./src/media/photos/2e_ch0_face.png)

International  support  is  available with this package.  Native language Manu pages are accessible (if available on your system) via use of locale functions.  To activate such support, it is necessary to set either `$LC_MES‐
SAGES`, `$LANG` or another system dependent environment variable to your language locale, usually specified in the `POSIX 1003.1` based format:

    <language>[_<territory>[.<character-set>[,<version>]]]

If the desired page is available in your locale, it will be displayed in lieu of the standard (usually American English) page.

Support for international message catalogues is also featured in this package and can be activated in the same way, again if available.  If you find that the Manu pages and message catalogues supplied with this  package  are
not available in your native language and you would like to supply them, please contact the maintainer who will be coordinating such activity.

For information regarding other features and extensions available with this Manu pager, please read the documents supplied with the package.

## Defaults

Manu will search for the desired Manu pages within the index database caches. If the -u option is given, a cache consistency check is performed to ensure the databases accurately reflect the filesystem.  If this option is al‐
ways given, it is not generally necessary to run mandb after the caches are initially created, unless a cache becomes corrupt.  However, the cache consistency check can be slow on systems with many Manu pages  installed,  so
it  is  not  performed  by default, and system administrators may wish to run mandb every week or so to keep the database caches fresh.  To forestall problems caused by outdated caches, Manu will fall back to file globbing if a
cache lookup fails, just as it would if no cache was present.

Once a Manu page has been located, a check is performed to find out if a relative preformatted `cat' file already exists and is newer than the nroff file.  If it does and is, this preformatted file is (usually)  decompressed
and  then  displayed, via use of a pager.  The pager can be specified in a number of ways, or else will fall back to a default is used (see option -P for details).  If no cat is found or is older than the nroff file, the nroff
is filtered through various programs and is shown immediately.

---

<a href ="index.html">

        ..          
       _||_______   
     .'/:/:||:\:\`. 
     :------------: 
     |  [] [] []  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |  __ __ __  | 
     |  || || ||  | 
     |  ~~ ~~ ~~  | 
     |__ ___ ___  | 
     ||| | | |=|  | 
    _||| ~~~ |_|  |_
    ░░░░░░░░░░░░░░░░

</a>
